import math
import random

T = 100000 * 2
deltaT = 100
counter = 0


def updateTemp():
    global counter
    global T
    # increases temp if stucked in local optimum
    if counter == 100:
        T += 100 * 50
        counter = 0
    else:
        T -= deltaT


def run(data, comparer):
    global T
    global counter
    global deltaT
    globalOpt = math.inf
    localOpt = prevLocalOpt = math.inf
    index = math.inf
    T = 100000 * 50
    deltaT = 100
    counter = 0

    while T != 0:
        if index == math.inf:
            index = random.randint(0, len(data) - 1)
        localOpt = data[index]
        minus = index - 1 if index - 1 != -1 else 0
        plus = index + 1 if index + 1 != len(data) else 0

        dataPlus = data[plus]
        dataMinus = data[minus]
        dataIndex = data[index]
        if min(data[plus], data[minus]) == data[plus] and comparer(data[plus], data[index]) < 0:
            # moves to index + 1 if it gives better solution
            index = plus
            localOpt = data[index]

         # moves to index - 1 if it gives better solution
        elif comparer(data[minus], data[index]) < 0:
            index = minus
            localOpt = data[index]
        else:
            P = math.pow(math.e, -(localOpt - prevLocalOpt) / T)
            # probablity if worse solution will be accepted
            if P > random.random():
                index = random.randint(0, len(data) - 1)
                localOpt = data[index]

        # updates best solution
        if comparer(localOpt, globalOpt) < 0:
            globalOpt = localOpt

        updateTemp()
        prevLocalOpt = localOpt
    return globalOpt
