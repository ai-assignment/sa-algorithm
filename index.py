import random
import math
import csv
import time
import sa

A = [[1, 4, 5, 12],
     [-5, 8, 9, 0],
     [-6, 7, 11, 19]]


def read_csv():
    with open('data2000.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        arr = []
        for row in csv_reader:
            arr.append(row)
            # if line_count == 0:
            #     print(f'Column names are {", ".join(row)}')
            #     line_count += 1
            # else:
            #     print(
            #         f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
            #     line_count += 1
            max = -math.inf
            start = time.time()
        # for row in arr:
        #     for item in row:
        #         if float(item) > max:
        #             max = float(item)
        # end = time.time()
        # print('Time: ', end - start)
        # print('Value: ', max)
        return arr


def floatComparer(a, b):
    if a < b:
        return -1
    if a > b:
        return 1
    return 0


newArr = []
arr = read_csv()
for row in arr:
    for item in row:
        newArr.append(float(item))

res = sa.run(newArr, floatComparer)
print(res)

quit(0)
